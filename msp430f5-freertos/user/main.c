
#include "msp430.h"
#include "driverlib.h"
#include "system_clock.h"
#include "timeA_drv.h"

#include "FreeRTOS.h"
#include "task.h"

volatile  uint16_t data=0; 
// 任务句柄
TaskHandle_t xTask1Handle = NULL;
TaskHandle_t xTask2Handle = NULL;

// 任务函数
void vTask1(void *pvParameters) {
    for (;;) {
        // 执行任务1的代码
        // 例如，切换LED状态或打印信息
      data=100;
    }
}

void vTask2(void *pvParameters) {
    for (;;) {
        // 执行任务2的代码
        // 例如，发送数据或打印信息
      data=2;
    }
}


void main( void )
{
  
  
  // Stop watchdog timer to prevent time out reset
  WDT_A_hold(WDT_A_BASE);  // 关闭看门狗 
  
  system_clock_init();
  // 创建任务1
    xTaskCreate(vTask1, "Task1", 256, NULL,
                tskIDLE_PRIORITY + 1, &xTask1Handle);

    // 创建任务2
    xTaskCreate(vTask2, "Task2", 256, NULL,
                tskIDLE_PRIORITY + 1, &xTask2Handle);

    // 启动调度器
    vTaskStartScheduler();

    for (;;);

}



/* The MSP430X port uses this callback function to configure its tick interrupt.
This allows the application to choose the tick interrupt source.
configTICK_VECTOR must also be set in FreeRTOSConfig.h to the correct
interrupt vector for the chosen tick interrupt source.  This implementation of
vApplicationSetupTimerInterrupt() generates the tick from timer A0, so in this
case configTICK_VECTOR is set to TIMER0_A0_VECTOR. */
void vApplicationSetupTimerInterrupt( void )
{
  timeA0_init((uint16_t)configTICK_RATE_HZ);
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* Called on each iteration of the idle task.  In this case the idle task
	just enters a low power mode. */
	//__bis_SR_register( LPM3_bits + GIE );
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues or
	semaphores. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pxTask;
	( void ) pcTaskName;

	/* Run time stack overflow checking is performed if
	configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
  
}