#ifndef USER_KEY_H_
#define USER_KEY_H_
#include "msp430f5438a.h"
#include "driverlib.h"
void key_init(void);
uint8_t key_scan(void);
#endif