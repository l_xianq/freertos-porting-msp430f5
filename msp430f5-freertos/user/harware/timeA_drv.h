#ifndef __TIMEA0_DRV_H__
#define __TIMEA0_DRV_H__
#include "msp430f5438a.h"
#include "driverlib.h"
void timeA0_init(uint16_t HZ);
void timeA1_init(void);
#endif