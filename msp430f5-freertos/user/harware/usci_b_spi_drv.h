#ifndef USCI_B_SPI_DRV_H_
#define USCI_B_SPI_DRV_H_
#include "msp430f5438a.h"
#include "driverlib.h"
#include <stdio.h>

#define SPI_CS_1  GPIO_setOutputHighOnPin(GPIO_PORT_P9, GPIO_PIN4)  
#define SPI_CS_0  GPIO_setOutputLowOnPin(GPIO_PORT_P9, GPIO_PIN4)  
void usci_b2_spi_init(void);
void spi_recv_buf(uint8_t *buf,uint8_t len);
void spi_send_buf(uint8_t *buf,uint8_t len);
uint8_t spi_send_recv_byte(uint8_t data);
void spi_cs(uint8_t flag);
#endif