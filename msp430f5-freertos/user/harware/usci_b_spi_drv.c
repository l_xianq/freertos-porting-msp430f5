#include "usci_b_spi_drv.h"
#include <stdio.h>
#include "user_delay.h"
#define SPICLK 9600
/*
    clk:p9.3
    mosi:p9.1
    miso:p9.2
    cs:p9.4
*/
void usci_b2_spi_init(void)
{   
    USCI_B_SPI_disable(USCI_B2_BASE);
    //复用引脚作spi功能
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P9,GPIO_PIN1|GPIO_PIN3);
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P9,GPIO_PIN2);
    //片选引脚初始化
    GPIO_setAsOutputPin(GPIO_PORT_P9,GPIO_PIN4);
    GPIO_setOutputHighOnPin(GPIO_PORT_P9,GPIO_PIN4);

    USCI_B_SPI_initMasterParam param = {0};
    param.selectClockSource = USCI_B_SPI_CLOCKSOURCE_ACLK;
    param.clockSourceFrequency = UCS_getACLK();
    param.desiredSpiClock = SPICLK;
    param.msbFirst = USCI_B_SPI_MSB_FIRST;
    param.clockPhase = USCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT;
    param.clockPolarity = USCI_B_SPI_CLOCKPOLARITY_INACTIVITY_LOW;

    uint8_t returnValue = USCI_B_SPI_initMaster(USCI_B2_BASE, &param);
    if (STATUS_FAIL == returnValue){
        return;
    }

    USCI_B_SPI_enable(USCI_B2_BASE);
    SPI_CS_1;
}


static void spi_send_byte(uint8_t data){
    USCI_B_SPI_transmitData(USCI_B2_BASE,data);
    while (!USCI_B_SPI_getInterruptStatus(USCI_B2_BASE,USCI_B_SPI_TRANSMIT_INTERRUPT));
    USCI_B_SPI_clearInterrupt(USCI_B2_BASE,USCI_B_SPI_TRANSMIT_INTERRUPT);

}

static uint8_t spi_recv_byte(void){
    uint8_t data;
    while (!USCI_B_SPI_getInterruptStatus(USCI_B2_BASE,USCI_B_SPI_RECEIVE_INTERRUPT));
    USCI_B_SPI_clearInterrupt(USCI_B2_BASE,USCI_B_SPI_RECEIVE_INTERRUPT);
    data=USCI_B_SPI_receiveData(USCI_B2_BASE);
    return data;
}

/*
    使用一下函数前，应选择片选;
    
    spi进行读取操作时也需要发送一个字节，才能接收一个字节，
    读取时还要发送字节是为了长生clk时钟，此处的spi的接收和发送是同时发生的。
    所以读取时，发送0xff,目的是产生时钟信号。

    注意：接收到的数据要及时读出，否则会导致数据丢失。
*/
uint8_t spi_send_recv_byte(uint8_t data){
    spi_send_byte(data);
    return spi_recv_byte();
}

void spi_recv_buf(uint8_t *buf,uint8_t len){
    for(uint8_t i=0;i<len;i++){
        buf[i]=spi_send_recv_byte(0xff);
    }
}

void spi_send_buf(uint8_t *buf,uint8_t len){
    for(uint8_t i=0;i<len;i++){
        spi_send_recv_byte(buf[i]);
    }
}

void spi_cs(uint8_t flag)
{
    if(flag){
        SPI_CS_0;
    }else{
        SPI_CS_1;
    }
}