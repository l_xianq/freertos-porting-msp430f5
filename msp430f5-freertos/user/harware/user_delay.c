#include "user_delay.h"
#include "system_clock.h"


void delay_3_NOP(void) {
    uint8_t i=3;
    while(i--);
}

void delay_us(uint32_t us)
{   
    uint32_t cpu=mclk_value;
    uint32_t count=cpu/1000000*us/10;
    for(uint32_t i=0; i<count; i++) ;
}
/*
    ms:最大延时时间为1999ms
*/
void timer_A0_delay_ms(uint16_t ms)
{   
    Timer_A_stop(TIMER_A0_BASE);
    if(ms>1999) return ;
    uint16_t count=ms*UCS_getACLK()/1000;

    Timer_A_initUpModeParam param={0};
    param.clockSource=TIMER_A_CLOCKSOURCE_ACLK;
    param.clockSourceDivider=TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod=count;
    param.timerInterruptEnable_TAIE=TIMER_A_TAIE_INTERRUPT_DISABLE;
    param.captureCompareInterruptEnable_CCR0_CCIE=TIMER_A_CCIE_CCR0_INTERRUPT_DISABLE;
    param.timerClear=TIMER_A_DO_CLEAR;
    param.startTimer=true; 
    Timer_A_initUpMode(TIMER_A0_BASE,&param);

    while(Timer_A_getCounterValue(TIMER_A0_BASE)<count);
    Timer_A_stop(TIMER_A0_BASE);
}