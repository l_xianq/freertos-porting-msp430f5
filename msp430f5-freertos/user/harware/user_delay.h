#ifndef USER_DELAY_H_
#define USER_DELAY_H_
#include <stdint.h>
#include "driverlib.h"
#include "msp430f5438a.h" 
void delay_3_NOP(void);
void delay_us(uint32_t us);
void timer_A0_delay_ms(uint16_t ms);

#ifdef MCLK_1MHz
    #define DELAY_US(us)    __delay_cycles((long)1*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)1000*(ms))
#elif  MCLK_6MHz
    #define DELAY_US(us)    __delay_cycles((long)6*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)6000*(ms))
#elif MCLK_9MHz
    #define DELAY_US(us)    __delay_cycles((long)9*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)9000*(ms))
#elif MCLK_12MHz
    #define DELAY_US(us)    __delay_cycles((long)12*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)12000*(ms))
#elif MCLK_18MHz
    #define DELAY_US(us)    __delay_cycles((long)18*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)18000*(ms))
#elif MCLK_25MHz
    #define DELAY_US(us)    __delay_cycles((long)25*(us))
    #define DELAY_MS(ms)    __delay_cycles((long)25000*(ms))
#endif


#endif