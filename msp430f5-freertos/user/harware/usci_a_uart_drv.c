#include "usci_a_uart_drv.h"
#include <stdio.h>

#define UART_BAUD_RATE_9600

#ifdef UART_BAUD_RATE_9600
    #define CLOCK_PRESCALAR 3
    #define FIRST_MOD_REG 0
    #define SECOND_MOD_REG 3
#elif UART_BAUD_RATE_4800
    #define CLOCK_PRESCALAR 6
    #define FIRST_MOD_REG 0
    #define SECOND_MOD_REG 7
#elif UART_BAUD_RATE_2400
    #define CLOCK_PRESCALAR 13
    #define FIRST_MOD_REG 0
    #define SECOND_MOD_REG 6
#elif UART_BAUD_RATE_1200
    #define CLOCK_PRESCALAR 27
    #define FIRST_MOD_REG 0
    #define SECOND_MOD_REG 2
#endif

/*
    RX： P10.5
    TX： P10.4
    时钟源：ACLK(32768HZ)
    RS485接收发送控制引脚：P2.0

*/
void usci_a3_uart_init(void)
{   
    USCI_A_UART_initParam param={0};
    param.selectClockSource=USCI_A_UART_CLOCKSOURCE_ACLK;
    param.parity=USCI_A_UART_NO_PARITY;
    param.msborLsbFirst=USCI_A_UART_LSB_FIRST;
    param.numberofStopBits=USCI_A_UART_ONE_STOP_BIT;
    param.uartMode=USCI_A_UART_MODE;
    param.overSampling=USCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
    param.clockPrescalar=CLOCK_PRESCALAR;
    param.firstModReg=FIRST_MOD_REG;
    param.secondModReg=SECOND_MOD_REG;
    USCI_A_UART_init(USCI_A3_BASE,&param);

    //配置端口为uart模式
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P10,GPIO_PIN5 );
    GPIO_setAsPeripheralModuleFunctionOutputPin( GPIO_PORT_P10,GPIO_PIN4);

    GPIO_setAsOutputPin(GPIO_PORT_P2,GPIO_PIN0);
    GPIO_setOutputHighOnPin(GPIO_PORT_P2,GPIO_PIN0);

    USCI_A_UART_enable(USCI_A3_BASE);

    // USCI_A_UART_enableInterrupt(USCI_A3_BASE,USCI_A_UART_RECEIVE_INTERRUPT);
}

#pragma vector=USCI_A3_VECTOR
__interrupt void USCI_A3_ISR(void)
{   
    static uint8_t count=0;
    static uint8_t rx_data=0;
    switch(UCA3IV)
    {
        case 0x02: {
            
            rx_data=USCI_A_UART_receiveData(USCI_A3_BASE);
            USCI_A_UART_clearInterrupt(USCI_A3_BASE,USCI_A_UART_RECEIVE_INTERRUPT_FLAG);
            break;
        }
        case 0x04:{
            count++;
            // USCI_A_UART_clearInterrupt(USCI_A3_BASE,USCI_A_UART_TRANSMIT_INTERRUPT_FLAG);
            break;
        }
    }
}


int putchar(int ch)
 
{
    USCI_A_UART_transmitData(USCI_A3_BASE,(uint8_t)ch);
    while(USCI_A_UART_getInterruptStatus(USCI_A3_BASE,USCI_A_UART_TRANSMIT_INTERRUPT_FLAG)==0);
    return ch;
}