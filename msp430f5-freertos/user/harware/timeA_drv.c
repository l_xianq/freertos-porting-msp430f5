#include  "timeA_drv.h"

void timeA0_init(uint16_t HZ)
{
    Timer_A_stop(TIMER_A0_BASE);

    Timer_A_initUpModeParam param={0};
    param.clockSource=TIMER_A_CLOCKSOURCE_ACLK;
    param.clockSourceDivider=TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod=32768/HZ;
    param.timerInterruptEnable_TAIE=TIMER_A_TAIE_INTERRUPT_DISABLE;
    param.captureCompareInterruptEnable_CCR0_CCIE=TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
    param.timerClear=TIMER_A_DO_CLEAR;
    param.startTimer=true; 
    Timer_A_initUpMode(TIMER_A0_BASE,&param);
}
void timeA1_init(void)
{
    Timer_A_stop(TIMER_A1_BASE);

    Timer_A_initUpModeParam param={0};
    param.clockSource=TIMER_A_CLOCKSOURCE_ACLK;
    param.clockSourceDivider=TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod=33000;
    param.timerInterruptEnable_TAIE=TIMER_A_TAIE_INTERRUPT_DISABLE;
    param.captureCompareInterruptEnable_CCR0_CCIE=TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
    param.timerClear=TIMER_A_DO_CLEAR;
    param.startTimer=true; 
    Timer_A_initUpMode(TIMER_A1_BASE,&param);
}

// #pragma vector=TIMER0_A0_VECTOR
// __interrupt void TimerA0_CC0_ISR (void) //CCR0
// {
//     static uint16_t count_A0=0;

//     count_A0++;
// }
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TimerA1_CC0_ISR (void) //CCR0
{
    static uint16_t count_A1=0;

    count_A1++;
    //LPM4_EXIT;
}