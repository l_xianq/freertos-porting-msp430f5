#include "user_key.h"
#include "user_delay.h"
#define KEY_PORT GPIO_PORT_P1
#define KEY_PIN GPIO_PIN2

#define GET_KEY_STATE() GPIO_getInputPinValue(KEY_PORT, KEY_PIN)

void key_init(void)
{
    GPIO_setAsInputPinWithPullUpResistor(KEY_PORT, KEY_PIN);
}

uint8_t key_scan(void)
{
    uint8_t key_state = 0;
    static uint8_t key_last_state = 0;

    key_state = GET_KEY_STATE();

    if(key_state == 0){
        delay_us(10000);
        if(key_state == 1) return 0;
        if(key_last_state == 1 && key_state == 0){
            key_last_state = key_state;
            return 1;
        }
    }
    key_last_state = key_state;
    return 0;
}