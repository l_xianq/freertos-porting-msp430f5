#include "rtc_drv.h"
#include "lcd.h"

void rtc_init(void)
{
    //设置初始日历
    Calendar calendar;
    calendar.Year = 0x2018;
    calendar.Month = 0x12;
    calendar.DayOfMonth=0x1;
    calendar.DayOfWeek=0x1;
    calendar.Hours=0x12;
    calendar.Minutes=0x31;
    calendar.Seconds=0x55;

    //关断RTC时钟
    RTC_A_holdClock(RTC_A_BASE);
    RTC_A_holdCounterPrescale(RTC_A_BASE,RTC_A_PRESCALE_0);
    RTC_A_holdCounterPrescale(RTC_A_BASE,RTC_A_PRESCALE_1);

    //初始化日历，并配置为BCD格式
    RTC_A_initCalendar(RTC_A_BASE,&calendar,RTC_A_FORMAT_BCD);
    //设置事件，每分钟溢出触发
    RTC_A_setCalendarEvent(RTC_A_BASE,RTC_A_CALENDAREVENT_MINUTECHANGE);
    
    //配置闹钟
    RTC_A_configureCalendarAlarmParam param;
    param.minutesAlarm=0x32;
    param.hoursAlarm=RTC_A_ALARMCONDITION_OFF;
    param.dayOfWeekAlarm=RTC_A_ALARMCONDITION_OFF;
    param.dayOfMonthAlarm=RTC_A_ALARMCONDITION_OFF;
    RTC_A_configureCalendarAlarm(RTC_A_BASE,&param);

    //清除中断标志位
    RTC_A_clearInterrupt(RTC_A_BASE,RTC_A_TIME_EVENT_INTERRUPT|RTC_A_CLOCK_ALARM_INTERRUPT|RTC_A_CLOCK_READ_READY_INTERRUPT);
    //使能中断:事件中断，闹钟中断，每秒更新日历完成中断
    RTC_A_enableInterrupt(RTC_A_BASE,RTC_A_TIME_EVENT_INTERRUPT|RTC_A_CLOCK_ALARM_INTERRUPT|RTC_A_CLOCK_READ_READY_INTERRUPT);

    //启动RTC时钟
    RTC_A_startClock(RTC_A_BASE);
    RTC_A_startCounterPrescale(RTC_A_BASE,RTC_A_PRESCALE_0);
    RTC_A_startCounterPrescale(RTC_A_BASE,RTC_A_PRESCALE_1);
}


#pragma vector=RTC_VECTOR
__interrupt void rtc_interrupt_server(void){
    switch(RTCIV){

        case 2:{
            //对日历更改完成中断
            static int count=0;
            count++;
            break;
        }
        case 4:{
            //事件中断
            static Calendar calendar={0};
            calendar=RTC_A_getCalendarTime(RTC_A_BASE);
            // Display_Date_Icon(calendar.Year,calendar.Month,calendar.DayOfMonth,calendar.Hours,calendar.Minutes);
            break;
        }
        case 6:{
            //闹钟中断
            static int time=0;
            time ++;
            break;
        }
        case 8:break;
        case 10:break;
        default:break;
    }

}