#include "system_clock.h"
#include "ucs.h"
#include "pmm.h"

/*
    此文件配置的开发板使用msp430f5438的内部
    时钟DCOCLK或DCOCLKDIV作为系统时钟源。
    XT1作用ACLK时钟源
    开发板上XT2没有外接晶振
*/ 

uint32_t aclk_value=32768; 
uint32_t mclk_value=0;
uint32_t smclk_value=0;

#ifdef MCLK_25MHz
void system_clock_25MHz(void);
#endif
#ifdef MCLK_18MHz
void system_clock_18MHz(void);
#endif
#ifdef MCLK_12MHz
void system_clock_12MHz(void);
#endif
#ifdef MCLK_9MHz
void system_clock_9MHz(void);
#endif
#ifdef MCLK_6MHz
void system_clock_6MHz(void);
#endif
#ifdef MCLK_1MHz
void system_clock_1MHz(void);
#endif


void system_clock_init(void)
{
    #ifdef MCLK_25MHz
    system_clock_25MHz();
    #endif
    #ifdef MCLK_18MHz
    system_clock_18MHz();
    #endif
    #ifdef MCLK_12MHz
    system_clock_12MHz();
    #endif
    #ifdef MCLK_9MHz
    system_clock_9MHz();
    #endif
    #ifdef MCLK_6MHz
    system_clock_6MHz();
    #endif
    #ifdef MCLK_1MHz
    system_clock_1MHz();
    #endif

    aclk_value = UCS_getACLK();
    mclk_value = UCS_getMCLK();
    smclk_value=UCS_getSMCLK();
}
/*
    UCS_initFLLSettle函数：如果设置的MCLK大于16M，则该函数会把SMCLK和
    MCLK的时钟源设置为DCOCLK，如果小于16M，则该函数会把SMCLK和MCLK的时钟源
    设置为DCOCLK分频后的DCOCLKDIV
*/
#ifdef MCLK_25MHz
void system_clock_25MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_3); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(25000,761);        //25000/32.768 ，设置DCOCLK的输出

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 = 25MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 25MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);



}
#endif

#ifdef MCLK_18MHz
void system_clock_18MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_2); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(18000,549);        //18000/32.768 ，设置DCOCLK的输出

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 = 25MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 25MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);

}
#endif


#ifdef MCLK_12MHz
void system_clock_12MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_1); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(12000,365);        //12000/32.768 ，设置DCOCLK的输出

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 = 12MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 12MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);

}
#endif


#ifdef MCLK_9MHz
void system_clock_9MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_1); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(9000,274);        //9000/32.768 ，设置DCOCLK的输出

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 = 9MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 9MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);

}
#endif


#ifdef MCLK_6MHz
void system_clock_6MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_0); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(6000,183);        //6000/32.768 

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 =6MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 6MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);

}
#endif





#ifdef MCLK_1MHz
void system_clock_1MHz(void)
{
    PMM_setVCore(PMM_CORE_LEVEL_0); 

    P7SEL |= BIT0 + BIT1;  //启用 XT1-32.768KHz ,选择作为晶振引脚

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_3,UCS_XCAP_3);    //开启XT1低速模式

    UCS_initClockSignal(UCS_FLLREF,UCS_XT1CLK_SELECT,UCS_CLOCK_DIVIDER_1);  //选择FLL参考频率

    UCS_initFLLSettle(1000,30);        //1000/32.768 ，设置DCOCLK的输出

    //XT1作为ACLK时钟源 = 32.768KHz
    UCS_initClockSignal(UCS_ACLK, UCS_XT1CLK_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为MCLK时钟源 = 1MHz
    UCS_initClockSignal(UCS_MCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //DCOCLK作为SMCLK时钟源 = 1MHz
    UCS_initClockSignal(UCS_SMCLK, UCS_DCOCLKDIV_SELECT, UCS_CLOCK_DIVIDER_1);

    //记录外部时钟源的频率
    //在调用UCS_getMCLK, UCS_getSMCLK 或 UCS_getACLK时可得到正确值
    UCS_setExternalClockSource(32768, 0);

}
#endif  