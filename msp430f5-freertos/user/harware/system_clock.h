#ifndef SYSTEM_CLOCK_H_
#define SYSTEM_CLOCK_H_
#include "msp430f5438a.h"
#include "driverlib.h"

#define MCLK_25MHz   
// #define MCLK_18MHz  
// #define MCLK_12MHz  
// #define MCLK_9MHz   
// #define MCLK_6MHz   
//#define MCLK_1MHz   

void system_clock_init(void);
extern uint32_t aclk_value; 
extern uint32_t mclk_value;
extern uint32_t smclk_value;
#endif